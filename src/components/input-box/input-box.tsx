import { Vue, Component, Prop, Model, Emit } from 'vue-property-decorator';
import './input-box.less';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

@Component({})
export default class InputBox extends Vue {

    /**
     * 双向绑定值
     * @type {any}
     * @memberof InputBox
     */
    @Model('change') readonly itemValue?: any;

    /**
     * placeholder值
     * @type {String}
     * @memberof InputBox
     */
    @Prop() public placeholder?: string;

    /**
     * 是否禁用
     * @type {boolean}
     * @memberof InputBox
     */
    @Prop() public disabled?: boolean;

    /**
     * 当前值
     *
     * @memberof InputBox
     */
    get CurrentVal() {
        return this.itemValue;
    }

    /**
     * 值变化
     *
     * @memberof InputBox
     */
    set CurrentVal(val: any) {
        this.inputDataChang.next(val);
    }

    /**
     * 失去焦点事件
     *
     * @param {*} event
     * @memberof InputBox
     */
    @Emit()
    public blur(event: any) {

    }

    /**
     * 获取焦点事件
     *
     * @param {*} event
     * @memberof InputBox
     */
    @Emit()
    public focus(event: any) {

    }

    /**
     * 值变化时间
     *
     * @private
     * @type {Subject<any>}
     * @memberof InputBox
     */
    private inputDataChang: Subject<any> = new Subject()

    /**
     * vue  声明周期 debounceTime
     *
     * @memberof InputBox
     */
    public created() {
        this.inputDataChang
            .pipe(
                debounceTime(500),
                distinctUntilChanged()
            ).subscribe((data: any) => {
                this.$emit('change', data);
            });
    }


    /**
     * 渲染组件
     *
     * @returns
     * @memberof InputBox
     */
    public render() {
        return (
            <i-input v-model={this.CurrentVal} disabled={this.disabled ? true : false} on-blur={this.blur} on-focus={this.focus} placeholder={this.placeholder}></i-input>
        );
    }
}
import { Vue, Component } from 'vue-property-decorator';
import './login.less';
import { Environment } from '@/environments/environment';

@Component({})
export default class Login extends Vue {

    /**
     * 表单对象
     *
     * @type {*}
     * @memberof Login
     */
    public form: any = { loginname: '', password: '' };

    /**
     * 登陆处理
     *
     * @memberof Login
     */
    public handleSubmit(): void {
        console.log(this.form);
        if (Object.is(this.form.loginname, '') || Object.is(this.form.password, '')) {
            return;
        }
        const post: Promise<any> = this.$http.post(Environment.RemoteLogin, this.form);
        post.then((response: any) => {
            if (response && response.status === 200) {
                const data = response.data;
                window.localStorage.setItem('token', data.token);
                console.log(this.$route.query);
                window.localStorage.setItem('user', JSON.stringify(data.user));
                const url: any = this.$route.query.redirect ? this.$route.query.redirect : '*';
                this.$router.push({ path: url });
            }
        }).catch((error: any) => {
            this.$Notice.error({ title: '错误', desc: '登陆失败' });
        });

    }

    /**
     * 内容绘制
     *
     * @returns
     * @memberof Login
     */
    public render() {
        return (
            <div class='login'>
                <div class='login-con'>
                    <card bordered={false}>
                        <p slot='title'>
                            <icon type='log-in'></icon>
                            欢迎登录
                    </p>
                        <div class='form-con'>
                            <i-form ref='loginForm'>
                                <form-item prop={'loginname'}>
                                    <i-input prefix={'ios-contact'} v-model={this.form.loginname} placeholder='请输入用户名'></i-input>
                                </form-item>
                                <form-item prop={'password'}>
                                    <i-input prefix={'ios-key'} v-model={this.form.password} type='password' placeholder='请输入密码'></i-input>
                                </form-item>
                                <form-item>
                                    <i-button on-click={this.handleSubmit} type='primary' long>登录</i-button>
                                </form-item>
                            </i-form>
                            <p class='login-tip'>输入任意用户名和密码即可</p>
                        </div>
                    </card>
                </div>
            </div>
        );
    }
}
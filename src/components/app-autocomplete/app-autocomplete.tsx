import { Component, Vue, Prop } from 'vue-property-decorator';
import './app-autocomplete.less';

@Component({})
export default class AppAutocomplete extends Vue {

    /**
     * 表单数据
     *
     * @type {*}
     * @memberof AppAutocomplete
     */
    @Prop() public activedata: any;

    /**
     * 是否启用
     *
     * @type {boolean}
     * @memberof AppAutocomplete
     */
    @Prop() public disabled?: boolean;

    /**
     * 属性项名称
     *
     * @type {string}
     * @memberof AppAutocomplete
     */
    @Prop() public name?: string;

    /**
     * 值项名称
     *
     * @type {string}
     * @memberof AppAutocomplete
     */
    @Prop() public valueitem?: string;

    /**
     * 值
     *
     * @type {*}
     * @memberof AppAutocomplete
     */
    @Prop() public value?: any;

    /**
     * 远程请求url 地址
     *
     * @type {string}
     * @memberof AppAutocomplete
     */
    @Prop() public url?: string;

    /**
     * 查询
     *
     * @param {*} query
     * @param {*} callback
     * @memberof AppAutocomplete
     */
    public onSearch(_query: any, callback: any): void {
        if (!this.url || (this.url && Object.is(this.url, ''))) {
            return;
        }
        const param: any = {
            srfaction: 'itemfetch',
            query: _query,
        };
        const url = `${this.url}${this.name}/ac`;
        if (this.activedata) {
            Object.assign(param, { srfactivedata: JSON.stringify(this.activedata) });
        }
        const post: Promise<any> = this.$http.post(url, param);
        post.then((response: any) => {
            let items: any[] = [];
            if (response.ret === 0 && response.data && Array.isArray(response.data)) {
                items = [...response.data];
            } else {
                this.$Notice.error({ title: '错误', desc: response.info });
            }
            callback(items);
        }).catch((error: any) => {
            this.$Notice.error({ title: '错误', desc: error.info });
        });
    }

    /**
     * 选中
     *
     * @param {*} item
     * @memberof AppAutocomplete
     */
    public onSelect(item: any) {
        if (this.name) {
            this.$emit('formitemvaluechange', { name: this.name, value: item.value });
        }
        if (this.valueitem) {
            this.$emit('formitemvaluechange', { name: this.valueitem, value: item.text });
        }
    }

    /**
     * 鼠标移出
     *
     * @param {*} e
     * @memberof AppAutocomplete
     */
    public onBlur(e: any) {
        let val: string = e.target.value;
        if (!Object.is(val, this.value)) {
            this.onSelect({ text: val, value: '' });
        }
    }

    /**
     * 清空选中项
     *
     * @memberof AppAutocomplete
     */
    public onClear() {
        if (this.name) {
            this.$emit('formitemvaluechange', { name: this.name, value: '' });
        }
        if (this.valueitem) {
            this.$emit('formitemvaluechange', { name: this.valueitem, value: '' });
        }
    }

    /**
     * 绘制内容
     *
     * @returns
     * @memberof AppAutocomplete
     */
    public render() {
        return (
            <el-autocomplete size='small' value={this.value} value-key='text' clearable
                fetch-suggestions={this.onSearch} on-select={($event: any) => this.onSelect($event)}
                on-blur={($event: any) => this.onBlur($event)} on-clear={() => this.onClear()}
                disabled={this.disabled} trigger-on-focus={false} style='width: 100%;'></el-autocomplete>
        );
    }
}
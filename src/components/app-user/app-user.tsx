import { Vue, Component } from 'vue-property-decorator';
import { Environment } from '@/environments/environment';

import './app-user.less';

@Component({})
export default class AppUser extends Vue {

    /**
     * 用户信息
     */
    public user = {
        name: '匿名访问',
        avatar: './assets/img/avatar.png',
    }

    /**
     * 下拉选选中回调
     * @param $event 
     */
    public userSelect(data: any) {
        if (Object.is(data, 'insrt')) {
            this.installRTData();
        } else {
            this.$Modal.confirm({
                title: '确认要退出登陆？',
                onOk: () => {
                    this.logout();
                }
            })
        }
    }

    /**
     * vue  生命周期
     *
     * @memberof AppUser
     */
    public mounted() {
        if (window.localStorage.getItem('user')) {
            const _user: any = window.localStorage.getItem('user') ? window.localStorage.getItem('user') : '';
            const user = JSON.parse(_user);
            Object.assign(this.user, user, {
                time: +new Date
            });
        }
        // const post: Promise<any> = this.$http.post(Environment.AppLogin, { srfaction: 'getcuruserinfo' });
        // post.then((action) => {
        //     if (action.ret === 0) {
        //         if (Object.keys(action.data).length !== 0) {
        //             let _data: any = {};
        //             Object.assign(_data, action.data);
        //             Object.assign(this.user, {
        //                 name: _data.username,
        //                 email: _data.loginname,
        //                 id: _data.userid,
        //                 time: +new Date
        //             });
        //         }
        //     }
        // }).catch((error: any) => {
        //     this.$Notice.error({ title: '错误', desc: error.info });
        // });
    }

    /**
     * 安装依赖
     */
    public installRTData() {
        const post: Promise<any> = this.$http.post(Environment.InstallRTData, {});
        post.then((action) => {
            if (action.ret === 0) {
                this.$Notice.success({ title: '成功', desc: action.info });
            }
            else {
                this.$Notice.error({ title: '错误', desc: action.info });
            }
        }).catch((error: any) => {
            this.$Notice.error({ title: '错误', desc: error.info });
        });
    }

    /**
     * 退出登录
     *
     * @memberof AppUser
     */
    public logout() {
        window.localStorage.removeItem('user');
        window.localStorage.removeItem('token');
        this.$router.push({ name: 'login' });
    }

    /**
     * 渲染组件
     */
    public render() {
        return (
            <div class="app-header-user">
                <dropdown on-on-click={this.userSelect}>
                    <div style="font-size: 15px;cursor: pointer;margin-right: 10px;padding:0 5px;">
                        <span>{this.user.name}</span>
                        <avatar src={this.user.avatar} />
                    </div>
                    <dropdown-menu slot="list" style="font-size: 15px !important;">
                        <dropdown-item name="insrt" style="font-size: 15px !important;">
                            <span> <i aria-hidden="true" class="fa fa-cogs" style="margin-right: 8px;"></i></span>
                            <span>安装依赖</span>
                        </dropdown-item>
                        <dropdown-item name="logout" style="font-size: 15px !important;">
                            <span> <i aria-hidden="true" class="fa fa-cogs" style="margin-right: 8px;"></i></span>
                            <span>退出登陆</span>
                        </dropdown-item>
                    </dropdown-menu>
                </dropdown>
            </div>
        )
    }
}
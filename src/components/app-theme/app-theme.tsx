import { Component, Vue } from 'vue-property-decorator';
import './app-theme.less';

@Component({})
export default class AppTheme extends Vue {

    /**
     * 所选择的主题
     */
    selectTheme: any = '';

    /**
     * 激活主题
     */
    public activeTheme: any;

    /**
     * 主题集合
     */
    defaultThemes: Array<any> = [
        {
            tag: 'app-default-theme',
            title: 'light',
            color: '#f6f6f6',
        },
        {
            title: 'Blue',
            tag: 'app_theme_blue',
            color: '#6ba1d1'
        },
        {
            title: 'Dark Blue',
            tag: 'app_theme_darkblue',
            color: '#606d80'
        }
    ];

    /**
     * 所选择的字体
     */
    public selectFont: any = '';

    /**
     * 字体集合
     */
    public fontFamilys = [
        {
            label: '微软雅黑',
            value: 'Microsoft YaHei',
        },
        {
            label: '黑体',
            value: 'SimHei',
        },
        {
            label: '幼圆',
            value: 'YouYuan',
        },
    ];

    /**
     * 挂载元素事件
     */
    public mounted() {
        if(localStorage.getItem('theme-class')){
            this.selectTheme = localStorage.getItem('theme-class');
        }else{
            this.selectTheme ='app-default-theme';
        }
        if(localStorage.getItem('font-family')){
            this.selectFont = localStorage.getItem('font-family');
        }else{
            this.selectFont ='Microsoft YaHei';
        }
    }

    public themeChange(val: any) {
        if (!Object.is(this.activeTheme, val)) {
            this.selectTheme = val;
            localStorage.setItem('theme-class',val);
            this.$router.app.$store.commit('setCurrentSelectTheme', val);
        }
    }

    public fontChange(val: any) {
        if (!Object.is(this.selectFont, val)) {
            this.selectFont = val;
            localStorage.setItem('font-family',val);
            this.$router.app.$store.commit('setCurrentSelectFont', val);
        }
    }

    public render() {
        return (<div class="app-theme">
            <poptip title="主题" popper-class="app-app-theme" placement="bottom-end">
                <a>
                    <icon class="app-theme-icon" type="md-settings" style="font-size:22px;"/>
                </a>
                <template slot="content">
                    <div class="app-theme-color">
                        {
                            this.defaultThemes.map((theme: any,index:any) => {
                                return <tooltip content={theme.title}>
                                    <div key={index} class={{'active':this.selectTheme==theme.tag,'app-theme-item':true}} style={{'background': theme.color}} on-click={() =>{this.themeChange(theme.tag)}}></div>
                                </tooltip>
                            })
                        }
                    </div>
                    <div>
                        <i-form label-width={60} label-position="left">
                            <form-item label="字体">
                                <i-select value={this.selectFont} size="small" style="width:100px" on-on-change={this.fontChange} transfer>
                                    {
                                        this.fontFamilys.map((font: any) => {
                                            return <i-option value={font.value} key={font.value}>{font.label}</i-option>
                                        })
                                    }
                                </i-select>
                            </form-item>
                        </i-form>
                    </div>
                </template>
            </poptip>
        </div>);
    }

}
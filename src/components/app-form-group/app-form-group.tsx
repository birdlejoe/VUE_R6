import { Vue, Component, Prop } from 'vue-property-decorator';
import './app-form-group.less';

@Component({})
export default class AppFormGroup extends Vue {

    /**
     * 标题
     *
     * @type {string}
     * @memberof AppFormGroup
     */
    @Prop() public caption?: string;

    /**
     * 内置界面样式
     * 
     * @type {string}
     * @memberof AppFormGroup
     */
    @Prop() public uiStyle?: string;

    /**
     * 是否显示标题
     *
     * @type {boolean}
     * @memberof AppFormGroup
     */
    @Prop({ default: true }) public isShowCaption!: boolean;

    /**
     * 内容绘制
     *
     * @memberof AppFormGroup
     */
    public render() {
        if (Object.is(this.uiStyle, 'DEFAULT')) {
            return (
                <div>
                    {this.isShowCaption ?
                        <card bordered={false} dis-hover={true}>
                            <p class='' slot='title'> {this.caption}</p>
                            <row gutter={10}>
                                {this.$slots.default}
                            </row>
                        </card> :
                        <row>
                            {this.$slots.default}
                        </row>}
                </div>
            );
        } else {
            return (
                <app-form-group2 caption={this.caption} uiStyle={this.uiStyle} isShowCaption={this.isShowCaption}>
                    {this.$slots.default}
                </app-form-group2>
            );
        }
    }
}
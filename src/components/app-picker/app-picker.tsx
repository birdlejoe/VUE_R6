import { Component, Vue, Prop, Model, Watch } from 'vue-property-decorator';
import './app-picker.less';
import { Subject } from 'rxjs';
import { AppModal } from '@/utils';

@Component({})
export default class AppPicker extends Vue {

    /**
     * 表单数据
     *
     * @type {*}
     * @memberof AppPicker
     */
    @Prop() public data!: any;

    /**
     * 属性项名称
     *
     * @type {string}
     * @memberof AppPicker
     */
    @Prop() public name!: string;

    /**
     * 是否启用
     *
     * @type {boolean}
     * @memberof AppPicker
     */
    @Prop() public disabled?: boolean;

    /**
     * 类型
     *
     * @type {string}
     * @memberof AppPicker
     */
    @Prop() public editortype?: string;

    /**
     * 视图名称
     *
     * @type {string}
     * @memberof AppPicker
     */
    @Prop() public refviewname?: string;

    /**
     * 视图参数（如：视图name，title，width，height）
     *
     * @type {*}
     * @memberof AppPicker
     */
    @Prop() public pickupView?: any;

    /**
     * 表单项参数
     * 
     * @type {any}
     * @memberof AppPicker
     */
    @Prop() public itemParam: any;

    /**
     * 值项名称
     *
     * @type {string}
     * @memberof AppPicker
     */
    @Prop() public valueitem?: string;

    /**
     * 值
     *
     * @type {*}
     * @memberof AppPicker
     */
    @Model('change') public value?: any;

    /**
     * 当前值
     *
     * @type {string}
     * @memberof AppPicker
     */
    public curvalue: string = '';

    /**   
     * 远程请求url 地址
     *
     * @type {string}
     * @memberof AppPicker
     */
    @Prop() public url?: string;

    /**
     * 下拉数组
     * @type {any[]}
     * @memberof AppPicker
     */
    public items: any[] = [];

    /** 
     * 下拉图标指向状态管理
     * @type {boolean}
     * @memberof AppPicker 
     */
    public open: boolean = false;

    /**
     * 获取关联数据项值
     *
     * @readonly
     * @memberof AppPicker
     */
    get refvalue() {
        if (this.valueitem && this.data) {
            return this.data[this.valueitem];
        }
        return this.curvalue;
    }

    /**
     * 值变化
     *
     * @param {*} newVal
     * @param {*} oldVal
     * @memberof AppPicker
     */
    @Watch('value')
    public onValueChange(newVal: any, oldVal: any) {
        this.curvalue = newVal;
    }

    /**
     * vue 生命周期
     *
     * @memberof AppPicker
     */
    public mounted() {
        if (Object.is(this.editortype, 'dropdown')) {
            this.onSearch('', null);
        }
    }

    /**
     * 下拉切换回调
     * @param flag 
     */
    public onSelectOpen(flag: boolean): void {
        this.open = flag;
        if (this.open) {
            this.onSearch(this.curvalue, null);
        }
    }

    /**
     * 执行搜索数据
     * @param query 
     * @param callback 
     */
    public onSearch(query: any, callback: any): void {

        query = !query ? '' : query;
        const url = `${this.url}${this.name}/ac`;
        let param: any = {};
        Object.assign(param, this.data);
        Object.assign(param, { [this.name]: query });

        this.$http.post(url, param).then((response: any) => {
            if (!response || response.status !== 200) {
                this.$Notice.error({ title: '错误', desc: '请求异常' });
            } else {
                this.items = [...response.data];
            }
            if (callback) {
                callback(this.items);
            }
        }).catch((error: any) => {
            if (callback) {
                callback([]);
            }
        });
    }

    /**
     * 选中数据回调
     * @param item 
     */
    public onACSelect(item: any): void {
        if (this.name) {
            this.$emit('formitemvaluechange', { name: this.name, value: item.text });
        }
        if (this.valueitem) {
            this.$emit('formitemvaluechange', { name: this.valueitem, value: item.value });
        }

    }

    /**
     * 下拉选中
     *
     * @param {string} val
     * @memberof AppPicker
     */
    public onSelect(val: string) {
        let index = this.items.findIndex((item) => Object.is(item.value, val));
        if (index >= 0) {
            this.onACSelect(this.items[index]);
        }
    }

    /**
     * 失去焦点事件
     * @param e 
     */
    public onBlur(e: any): void {
        let val: string = e.target.value;
        if (!Object.is(val, this.curvalue)) {
            this.onACSelect({ text: val, value: '' });
        }
        this.$forceUpdate();
    }

    /**
     * 清除
     */
    public onClear($event: any): void {
        if (this.name) {
            this.$emit('formitemvaluechange', { name: this.name, value: '' });
        }
        if (this.valueitem) {
            this.$emit('formitemvaluechange', { name: this.valueitem, value: '' });
        }
        this.$forceUpdate();
    }

    /**
     * 打开视图
     */
    public openView($event: any): void {
        if (this.disabled) {
            return;
        }
        // 填充条件判断
        let arg: any = {};
        const bcancel: boolean = this.fillPickupCondition(arg);
        if (!bcancel) {
            return;
        }

        let data = { srfparentdata: arg };
        const view = { ...this.pickupView };
        let modalContainer: Subject<any>;
        if (view.placement && !Object.is(view.placement, '')) {
            if (Object.is(view.placement, 'POPOVER')) {
                modalContainer = this.$apppopover.openPop($event, view, data);
            } else {
                modalContainer = this.$appdrawer.openDrawer(view, data);
            }
        } else {
            modalContainer = AppModal.getInstance().openModal(view, data);
        }
        modalContainer.subscribe((result: any) => {
            if (!result || !Object.is(result.ret, 'OK')) {
                return;
            }

            let item: any = {};
            if (result.datas && Array.isArray(result.datas)) {
                Object.assign(item, result.datas[0]);
            }

            if (this.data) {
                if (this.name) {
                    this.$emit('formitemvaluechange', { name: this.name, value: item.srfmajortext });
                }
                if (this.valueitem) {
                    this.$emit('formitemvaluechange', { name: this.valueitem, value: item.srfkey });
                }
            }
        })
    }

    /**
     * 填充表单条件
     *
     * @param {*} arg
     * @returns
     * @memberof AppPicker
     */
    public fillPickupCondition(arg: any): boolean {
        if (!this.itemParam) {
            return true;
        }
        if (!this.data) {
            this.$Notice.error({ title: '错误', desc: '表单数据异常' });
            return false;
        }
        if (this.itemParam.parentdata) {
            return Object.keys(this.itemParam.parentdata).every((name: string) => {
                let value: string = this.itemParam.parentdata[name];
                if (value.startsWith('%') && value.endsWith('%')) {
                    const key: string = value.substring(1, value.length - 1);
                    if (!this.data[key]) {
                        this.$Notice.error({ title: '错误', desc: `操作失败,未能找到当前表单项${key}，无法继续操作` });
                        return false;
                    }
                    value = `${this.data[key]}`;
                }
                Object.assign(arg, { [name]: value });
                return true;
            });
        }
        return true;
    }

    /**
     * 其他绘制
     *
     * @returns
     * @memberof AppPicker
     */
    public renderOther() {
        return (
            <div class='app-picker'>
                <el-autocomplete class='text-value' value-key='text' disabled={this.disabled} v-model={this.curvalue} size='small'
                    trigger-on-focus={false} fetch-suggestions={(query: any, callback: any) => { this.onSearch(query, callback) }} on-select={(item: any) => { this.onACSelect(item) }}
                    on-blur={($event: any) => { this.onBlur($event) }} style='width:100%;'>
                    <template slot='suffix'>
                        {(this.curvalue && !this.disabled) ? <i class='el-icon-circle-close' on-click={($event: any) => { this.onClear($event) }}></i> : ''}
                        {!Object.is(this.editortype, 'ac') ? <i class='el-icon-search' on-click={($event: any) => { this.openView($event) }}></i> : ''}
                    </template >
                </el-autocomplete >
            </div >
        );
    }

    /**
     * 绘制选择无ac
     *
     * @returns
     * @memberof AppPicker
     */
    public renderPickupNoAC() {
        return (
            <div class='app-picker'>
                <el-input class='text-value' value={this.curvalue} readonly size='small' disabled={this.disabled}>
                    <template slot='suffix'>
                        {(this.curvalue && !this.disabled) ? <i class='el-icon-circle-close' on-click={($event: any) => this.onClear($event)}></i> : ''}
                        <i class='el-icon-search' on-click={($event: any) => this.openView($event)}></i>
                    </template>
                </el-input>
            </div >
        );
    }

    /**
     * 绘制下拉
     *
     * @returns
     * @memberof AppPicker
     */
    public renderDropdown() {
        return (
            <div class='app-picker'>
                <el-select remote remote-method={(query: any) => this.onSearch(query, null)} value={this.refvalue} size='small' filterable
                    on-change={($event: any) => this.onSelect($event)} disabled={this.disabled} style='width:100%;' clearable
                    on-clear={($event: any) => this.onClear($event)} on-visible-change={($event: any) => this.onSelectOpen($event)}>
                    {this.items ? this.items.map((_item: any) => {
                        return <el-option value={_item.value} label={_item.text} disabled={_item.disabled}></el-option>;
                    }) : ''}
                </el-select>
                <span style='position: absolute;right: 5px;color: #c0c4cc;top:0;font-size: 13px;'>
                    {this.open ? <i class='el-icon-arrow-up'></i> : <i class='el-icon-arrow-down'></i>}
                </span>
            </div >
        );
    }

    /**
     * 绘制内容
     *
     * @memberof AppPicker
     */
    public render() {
        if (!Object.is(this.editortype, 'pickup-no-ac') && !Object.is(this.editortype, 'dropdown')) {
            return this.renderOther();
        } else if (Object.is(this.editortype, 'pickup-no-ac')) {
            return this.renderPickupNoAC();
        } else if (Object.is(this.editortype, 'dropdown')) {
            return this.renderDropdown();
        }
    }
}
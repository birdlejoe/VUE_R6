import { Vue, Component, Prop, } from 'vue-property-decorator';
import './app-form-group2.less';

@Component({})
export default class AppFormGroup2 extends Vue {

    /**
     * 标题
     *
     * @type {string}
     * @memberof AppFormGroup2
     */
    @Prop() public caption?: string;

    /**
     * 内置界面样式
     * 
     * @type {string}
     * @memberof AppFormGroup2
     */
    @Prop() public uiStyle?: string;

    /**
     * 是否显示标题
     *
     * @type {boolean}
     * @memberof AppFormGroup2
     */
    @Prop({ default: true }) public isShowCaption!: boolean;

    public unfoldAll = '1';

    /**
     * 绘制内容
     *
     * @returns
     * @memberof AppFormGroup2
     */
    public render() {
        return (
            <span>未实现</span>
        );
    }
}
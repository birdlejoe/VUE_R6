import { Vue, Component, Prop } from 'vue-property-decorator';
import './app-form-item2.less';

@Component({})
export default class AppFormItem2 extends Vue {

    /**
     * 名称
     *
     * @type {string}
     * @memberof AppFormItem2
     */
    @Prop() public caption!: string;

    /**
     * 标签位置
     *
     * @type {string}
     * @memberof AppFormItem2
     */
    @Prop() public labelPos?: string;

    /**
     * 标签宽度
     *
     * @type {number}
     * @memberof AppFormItem2
     */
    @Prop({}) public labelWidth!: number;

    /**
     * 是否显示标题
     *
     * @type {boolean}
     * @memberof AppFormItem2
     */
    @Prop() public isShowCaption?: boolean;

    /**
     * 标签是否空白
     *
     * @type {boolean}
     * @memberof AppFormItem2
     */
    @Prop() public isEmptyCaption?: boolean;

    /**
     * 表单项名称
     *
     * @type {string}
     * @memberof AppFormItem2
     */
    @Prop() public name!: string;

    /**
     * 内置样式
     *
     * @type {string}
     * @memberof AppFormItem2
     */
    @Prop() public uiStyle?: string;

    /**
     * 绘制内容
     *
     * @returns
     * @memberof AppFormItem2Style2
     */
    public render() {
        return (
            <span>未实现</span>
        );
    }
}
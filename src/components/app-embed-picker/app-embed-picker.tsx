import { Vue, Component, Prop } from 'vue-property-decorator';

import './app-embed-picker.less';
import { CreateElement } from 'vue';

@Component({})
export default class AppEmbedPicker extends Vue {

    /**
     * 值项名称
     *
     * @type {string}
     * @memberof AppPicker
     */
    @Prop() public valueItem?: string;

    /**
     * 关联视图名称
     *
     * @type {string}
     * @memberof AppPicker
     */
    @Prop() public refviewname?: string;

    /**
     * 空值文本
     *
     * @type {string}
     * @memberof EmbedPicker
     */
    @Prop() public emptyText?: string;

    /**
     * 属性项名称
     *
     * @type {string}
     * @memberof AppPicker
     */
    @Prop() public name?: string;

    mounted() {
        this.initComponent();
    }

    /**
     * 初始化组件
     *
     * @memberof AppEmbedPicker
     */
    public initComponent() {

    }

    /**
     * 设置值
     *
     * @param {*} item
     * @memberof AppEmbedPicker
     */
    public setValue(item: any) {
        if (this.name) {
            this.$emit('formitemvaluechange', { name: this.name, value: item[0].srfmajortext });
        }
        if (this.valueItem) {
            this.$emit('formitemvaluechange', { name: this.valueItem, value: item[0].srfkey });
        }
    }

    public render(h: CreateElement) {
        if(this.refviewname) {
            return (h(this.refviewname, {
                on: {
                    'viewdataschange': (args: any) => {
                        if(args && args.length > 0) {
                            this.setValue(args);
                        }
                    }
                }
            }));
        } else {
            return (<div>{this.emptyText}</div>);
        }
    }
}
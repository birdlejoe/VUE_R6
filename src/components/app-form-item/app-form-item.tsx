import { Vue, Component, Prop } from 'vue-property-decorator';

import './app-form-item.less';

@Component({})
export default class AppFormItem extends Vue {

    /**
     * 名称
     *
     * @type {string}
     * @memberof AppFormItem
     */
    @Prop() public caption!: string;

    /**
     * 错误信息
     *
     * @type {string}
     * @memberof AppFormItem
     */
    @Prop() public error?: string;

    /**
     * 标签位置
     *
     * @type {string}
     * @memberof AppFormItem
     */
    @Prop() public labelPos?: string;

    /**
     * 标签宽度
     *
     * @type {number}
     * @memberof AppFormItem
     */
    @Prop({}) public labelWidth!: number;

    /**
     * 是否显示标题
     *
     * @type {boolean}
     * @memberof AppFormItem
     */
    @Prop() public isShowCaption?: boolean;

    /**
     * 标签是否空白
     *
     * @type {boolean}
     * @memberof AppFormItem
     */
    @Prop() public isEmptyCaption?: boolean;

    /**
     * 表单项名称
     *
     * @type {string}
     * @memberof AppFormItem
     */
    @Prop() public name!: string;

    /**
     * 内置样式
     *
     * @type {string}
     * @memberof AppFormItem
     */
    @Prop() public uiStyle?: string;

    /**
     * 绘制内容
     *
     * @returns
     * @memberof AppFormItem
     */
    public render() {
        if (Object.is(this.uiStyle, 'DEFAULT')) {
            return (
                <form-item class='app-form-item' prop={this.name} error={this.error}
                    label-width={this.isShowCaption ? this.labelWidth : 0}>
                    {
                        this.isShowCaption && this.labelWidth > 0 ?
                            <span slot='label'>
                                {this.isEmptyCaption ? '' : this.caption}
                            </span>
                            : ''
                    }
                    {this.$slots.default}
                </form-item>

            );
        } else {
            return (
                <app-form-item2 caption={this.caption} labelPos={this.labelPos} labelWidth={this.labelWidth}
                    isShowCaption={this.isShowCaption} isEmptyCaption={this.isEmptyCaption}
                    name={this.name} uiStyle={this.uiStyle}>
                    {this.$slots.default}
                </app-form-item2>
            );
        }
    }
}
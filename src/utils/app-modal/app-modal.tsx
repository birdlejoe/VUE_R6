import Vue from 'vue';
import { Subject } from 'rxjs';
import store from '../../store';

import './app-modal.less';

export class AppModal {

    /**
     * 实例对象
     *
     * @private
     * @static
     * @memberof AppModal
     */
    private static modal = new AppModal();

    /**
     * vue 实例
     *
     * @private
     * @type {Vue}
     * @memberof AppModal
     */
    private vueExample!: Vue;

    /**
     * Creates an instance of AppModal.
     * 
     * @memberof AppModal
     */
    private constructor() {
        if (AppModal.modal) {
            return AppModal.modal;
        }
    }

    /**
     * 获取单例对象
     *
     * @static
     * @returns {AppModal}
     * @memberof AppModal
     */
    public static getInstance(): AppModal {
        if (!AppModal.modal) {
            AppModal.modal = new AppModal();
        }
        return AppModal.modal;
    }

    /**
     * 创建 Vue 实例对象
     *
     * @private
     * @param {{ viewname: string, title: string, width?: number, height?: number }} view
     * @param {*} [data={}]
     * @param {string} uuid
     * @returns {Subject<any>}
     * @memberof AppModal
     */
    private createVueExample(view: { viewname: string, title: string, width?: number, height?: number }, data: any = {}, uuid: string): Subject<any> {
        let subject = new Subject<any>();
        const div = document.createElement('div');
        div.setAttribute('id', uuid);
        document.body.appendChild(div);
        const vueExample = new Vue({
            store: store,
            el: div,
            data() {
                let data = {
                    isShow: false,
                    isfullscreen: false,
                    tempResult: { ret: '' },
                    viewname: '',
                    title: '',
                    width: 0,
                    currentViewLevel: 0,
                    height: {},
                }
                return data;
            },
            created() {
                this.viewname = view.viewname;
                this.title = view.title;
                if ((!view.width || view.width === 0 || Object.is(view.width, '0px'))) {
                    let width = 600;
                    if (window && window.innerWidth > 100) {
                        if (window.innerWidth > 100) {
                            width = window.innerWidth - 100;
                        } else {
                            width = window.innerWidth;
                        }
                    }
                    this.width = width;
                } else {
                    this.width = view.width;
                }
            },
            mounted() {
                this.isShow = true;
                const refs: any = this.$refs;
                if (!localStorage.getItem('viewlevel')) {
                    localStorage.setItem('viewlevel', '150');
                    this.currentViewLevel = 150 - refs.curmodal.modalIndex - 1;
                } else {
                    let computedViewLevel = Number(localStorage.getItem('viewlevel')) + 1;
                    localStorage.setItem('viewlevel', String(computedViewLevel));
                    this.currentViewLevel = computedViewLevel - refs.curmodal.modalIndex - 1;
                }
                // this.$el.getElementsByClassName('ivu-modal-mask')[0].style.zIndex =this.currentViewLevel.toString();
                // this.$el.getElementsByClassName('ivu-modal-wrap')[0].style.zIndex =this.currentViewLevel.toString();
                // console.log(this.currentViewLevel);
            },
            methods: {
                close: function (result: any) {
                    if (result && Array.isArray(result) && result.length > 0) {
                        Object.assign(this.tempResult, { datas: JSON.parse(JSON.stringify(result)) });
                    }
                    this.isShow = false;
                },
                dataChange: function (result: any) {
                    this.tempResult = { ret: '' };
                    if (result && Array.isArray(result) && result.length > 0) {
                        Object.assign(this.tempResult, { ret: 'OK' }, { datas: JSON.parse(JSON.stringify(result)) });
                    }
                },
                onVisibleChange: function ($event: any) {
                    if ($event) {
                        return;
                    }
                    if (subject) {
                        if (this.tempResult && Object.is(this.tempResult.ret, 'OK')) {
                            subject.next(this.tempResult);
                        }
                    }
                    setTimeout(() => {
                        vueExample.$destroy();
                        document.body.removeChild(vueExample.$el);
                    }, 500)
                }
            },
            render() {
                return (
                    <modal ref="curmodal" v-model={this.isShow} z-index={this.currentViewLevel} fullscreen={this.isfullscreen} title={this.title} footer-hide={true} mask-closable={false} width={this.width} style={this.height}
                        on-on-visible-change={($event: any) => this.onVisibleChange($event)} class-name="app-modal">
                        {this.viewname && !Object.is(this.viewname, '') ?
                            this.$createElement(this.viewname, {
                                class: {
                                    viewcontainer2: true,
                                },
                                props: {
                                    viewdata: JSON.stringify(data)
                                },
                                on: {
                                    viewdataschange: ($event: any) => this.dataChange($event),
                                    close: ($event: any) => this.close($event)
                                },
                            }) : ''}
                    </modal>
                );
            }
        });
        this.vueExample = vueExample;
        return subject;
    }

    /**
     * 打开模态视图
     *
     * @param {{ viewname: string, title: string, width?: number, height?: number }} view
     * @param {*} [data={}]
     * @returns {Subject<any>}
     * @memberof AppModal
     */
    public openModal(view: { viewname: string, title: string, width?: number, height?: number }, data: any = {}): Subject<any> {
        try {
            const uuid = this.getUUID();
            const subject = this.createVueExample(view, data, uuid);
            return subject;
        } catch (error) {
            console.log(error);
            return new Subject<any>();
        }
    }

    /**
     * 获取节点标识
     *
     * @private
     * @returns {string}
     * @memberof AppModal
     */
    private getUUID(): string {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    }

}
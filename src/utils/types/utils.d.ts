import { PluginFunction } from 'vue';

interface utils extends PluginFunction<any> { }

declare const utils: utils;
export default utils;

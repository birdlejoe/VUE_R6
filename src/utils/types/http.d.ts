import Vue, { VNode, CreateElement } from "vue";

/**
 * Http net 对象
 *
 * @export
 * @interface Http
 */
export declare interface Http {
    /**
     * post请求
     *
     * @param {string} url
     * @param {*} params  Object 对象
     * @returns {Promise<any>}
     * @memberof Http
     */
    post(url: string, params: any): Promise<any>;
    /**
     * post请求,不处理loading加载
     *
     * @param {string} url
     * @param {*} params Object 对象
     * @returns {Promise<any>}
     * @memberof Http
     */
    post2(url: string, params: any): Promise<any>;
    /**
     * 多参数
     *
     * @param {string} url
     * @param {*} params Object 对象
     * @returns {Promise<any>}
     * @memberof Http
     */
    get(url: string, params: any): Promise<any>;
    /**
     * url 处理参数
     *
     * @param {string} url
     * @returns {Promise<any>}
     * @memberof Http
     */
    get2(url: string): Promise<any>;
}

declare module "vue/types/vue" {
    interface Vue {
        /**
         * Http net 对象
         *
         * @type {Http}
         * @memberof Vue
         */
        $http: Http;
    }
}
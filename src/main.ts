import Vue from 'vue';
import Vuex from 'vuex';
import VueRouter from 'vue-router';
import App from './App';
import ElementUi from 'element-ui';
import iView from 'iview';
import 'element-ui/lib/theme-chalk/index.css';
import 'iview/dist/styles/iview.css';

import './styles/default.less';

// import utils from './utils';
import { AppComponents } from './app-register';
import { PageComponents } from './page-register';
import { UserComponent } from './user-register';
import store from './store';
import router from './router';

Vue.config.productionTip = false;

Vue.use(Vuex);
Vue.use(VueRouter);;
Vue.use(ElementUi);
Vue.use(iView);

// Vue.use(utils);
Vue.use(AppComponents);
Vue.use(PageComponents);
Vue.use(UserComponent);

router.beforeEach((to: any, from: any, next: any) => {
  setTimeout(() => {
    router.app.$store.commit('addPage', to);
  }, 10);
  next();
});

new Vue({
  store,
  router,
  render: (h: any) => h(App),
}).$mount('#app');
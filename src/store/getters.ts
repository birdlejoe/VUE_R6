/**
 * 获取代码表对象
 * 
 * @param state 
 */
export const getCodeList = (state: any) => (srfkey: string) => {
    return state.codelists.find((_codelist: any) => Object.is(_codelist.srfkey, srfkey));
}

/**
 * 获取代码表
 * 
 * @param state 
 */
export const getCodeListItems = (state: any) => (srfkey: string) => {
    let items: any[] = [];
    const codelist = state.codelists.find((_codelist: any) => Object.is(_codelist.srfkey, srfkey));
    if (!codelist) {
        console.log(`----${srfkey}----代码表不存在`);
    } else {
        items = [...codelist.items];
    }
    return items;
}

/**
 * 获取应用数据
 * 
 * @param state 
 */
export const getAppData = (state: any) => () => {
    return state.appdata;
}

/**
 * 获取本地应用数据
 * 
 * @param state 
 */
export const getLocalData = (state: any) => () => {
    return state.localdata;
}

/**
 * 根state
 */
export const rootstate: any = {
    pageTagList: [],
    pageMetas: [],
    historyPathList: [],
    codelists: [],
    selectTheme: '',
    selectFont: '',
    appdata: '',
    localdata: {},
}
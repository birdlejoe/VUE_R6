/**
 * 获取应用视图数据
 * 
 * @param state 
 */
export const getAppView = (state: any) => (viewtag: string) => {
    const appview = state.appviews.find((appview: any) => Object.is(appview.viewtag, viewtag));
    if (!appview) {
        console.log(`----视图 ${viewtag} 不存在-----`)
        return null;
    }
    return appview;
}

export const getViewAction = (state: any) => (viewtag: string) => {
    const appview = state.appviews.find((appview: any) => Object.is(appview.viewtag, viewtag));
    if (!appview) {
        console.log(`----视图 ${viewtag} 不存在-----`)
        return null;
    }
    return appview.viewaction;
}
/**
 * 添加关联视图
 * 
 * @param state 
 * @param param1 
 */
export const addRefView = (state: any, { viewtag, refviewtag }: { viewtag: string, refviewtag: string }) => {
    const appview = state.appviews.find((appview: any) => Object.is(appview.viewtag, viewtag));
    if (!appview) {
        console.log(`----视图标识 ${viewtag} 不存在-----`)
        return;
    }
    appview.refviews.push(refviewtag);
}

/**
 * 删除关系视图
 * 
 * @param state 
 * @param param1 
 */
export const removeRefView = (state: any, { viewtag, refviewtag }: { viewtag: string, refviewtag: string }) => {
    const appview = state.appviews.find((appview: any) => Object.is(appview.viewtag, viewtag));
    if (!appview) {
        console.log(`----视图标识 ${viewtag} 不存在-----`)
        return;
    }
    const index = appview.refviews.findIndex((_refviewtag: string) => Object.is(_refviewtag, refviewtag));
    if (index === -1) {
        console.log(`----关联视图 ${refviewtag} 不存在-----`)
        return;
    }
    appview.refviews.splice(index, 1);
}

/**
 * 设置视图数据变化状态
 * 
 * @param state 
 * @param param1 
 */
export const setViewDataChange = (state: any, { viewtag, viewdatachange }: { viewtag: string, viewdatachange: boolean }) => {
    const appview = state.appviews.find((appview: any) => Object.is(appview.viewtag, viewtag));
    if (!appview) {
        console.log(`----视图标识 ${viewtag} 不存在-----`)
        return;
    }
    appview.viewdatachange = viewdatachange;
}


/**
 * 设置视图行为
 * 
 * @param state 
 * @param param1 
 */
export const setViewAction = (state: any, { viewtag, viewaction }: { viewtag: string, viewaction: string }) => {
    const appview = state.appviews.find((appview: any) => Object.is(appview.viewtag, viewtag));
    if (!appview) {
        console.log(`----视图标识 ${viewtag} 不存在-----`)
        return;
    }
    appview.viewaction = viewaction;
}

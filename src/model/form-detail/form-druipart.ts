import { FormDetailModel } from './form-detail';

/**
 * 数据关系界面模型
 *
 * @export
 * @class FromDRUIPartModel
 * @extends {FormDetailModel}
 */
export class FromDRUIPartModel extends FormDetailModel {

    
    constructor(opts: any = {}) {
        super(opts);
    }
}
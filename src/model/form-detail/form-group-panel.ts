import { FormDetailModel } from './form-detail';

/**
 * 分组面板模型
 *
 * @export
 * @class FormGroupPanelModel
 * @extends {FormDetailModel}
 */
export class FormGroupPanelModel extends FormDetailModel {


    constructor(opts: any = {}) {
        super(opts);
    }
}
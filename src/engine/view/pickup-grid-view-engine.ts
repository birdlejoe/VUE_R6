import GridViewEngine from './grid-view-engine';

/**
 * 实体选择表格视图（部件视图）界面引擎
 *
 * @export
 * @class PickupGridViewEngine
 * @extends {GridViewEngine}
 */
export default class PickupGridViewEngine extends GridViewEngine {

    /**
     * Creates an instance of PickupGridViewEngine.
     * 
     * 
     * @memberof PickupGridViewEngine
     */
    constructor() {
        super();
    }
}